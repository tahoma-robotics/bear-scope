# bear-scope

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/org.tahomarobotics/bear-scope/badge.svg)](https://maven-badges.herokuapp.com/maven-central/org.tahomarobotics/bear-scope)

### Using the Dashboard
Use the `-Dsimulated=true` option to use with the simulator,  
otherwise specify which team you are using with the `-Dteam=####` option.  

Example commandline prompt:  

simulated-robot
-----------------
linux:> ./target/appassembler/bin/bear-scope-sim.sh
windows:> ./target/appassembler/bin/bear-scope-sim.bat

real-robot
-----------------
linux:> ./target/appassembler/bin/bear-scope.sh
windows:> ./target/appassembler/bin/bear-scope.bat
