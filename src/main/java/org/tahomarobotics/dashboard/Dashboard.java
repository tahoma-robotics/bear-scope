/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.dashboard;

import edu.wpi.first.networktables.NetworkTableInstance;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class Dashboard extends Application {



    @Override
    public void start(Stage primaryStage) {

        primaryStage.setTitle("Bear Scope");

        // setup Menu
        final Menu fileMenu = new Menu("File");
        MenuItem quitMenuItem = new MenuItem("Quit");
        quitMenuItem.setOnAction((event) -> System.exit(0));
        fileMenu.getItems().add(quitMenuItem);
        final Menu optionsMenu = new Menu("Options");
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu, optionsMenu);

        List<InstrumentView> views = new ArrayList<>();

        // Create tab pane and tabs
        final TabPane tabPane = new TabPane();
        InstrumentView view;

        view = new FieldView();
        views.add(view);
        tabPane.getTabs().add(new Tab("Field", view.getView()));

        view = new ChassisView();
        views.add(view);
        tabPane.getTabs().add(new Tab("Chassis", view.getView()));

        // combine and show
        VBox vBox = new VBox();
        vBox.getChildren().addAll(menuBar, tabPane);
        vBox.setFillWidth(true);
        VBox.setVgrow(tabPane, Priority.ALWAYS);
        HBox.setHgrow(tabPane, Priority.ALWAYS);
        Scene scene = new Scene(vBox);
        primaryStage.setScene(scene);
        primaryStage.show();

        new AnimationTimer() {
            public void handle(long currentNanoTime) {
                for(InstrumentView view : views) {
                    view.updateData(currentNanoTime);
                }
            }
        }.start();


        String serverName = System.getProperty("server", "localhost");

        NetworkTableInstance.getDefault().startClient(serverName);


        primaryStage.setOnCloseRequest(e -> System.exit(0));
    }


    public static void main(String[] args) {
        launch(args);
    }
}
