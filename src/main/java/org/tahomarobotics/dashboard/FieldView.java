/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.dashboard;

import edu.wpi.first.networktables.*;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;
import org.tahomarobotics.robot.util.PathData;

import java.util.function.Consumer;

public class FieldView extends StackPane implements InstrumentView {

	private static final double FIELD_WIDTH = 324.0;
	private static final double FIELD_LENGHT = 648.0;

	private static final int ROBOT_HEIGHT_PIXEL = 428;
	private static final int ROBOT_WIDTH_PIXEL = 372;
	private static final double ROBOT_WIDTH = 34.314;
	private static final double ROBOT_IMAGE_SCALE = ROBOT_WIDTH / ROBOT_WIDTH_PIXEL;
	private static final double ROBOT_X_OFFSET = ROBOT_IMAGE_SCALE * ROBOT_HEIGHT_PIXEL / 2;
	private static final double ROBOT_Y_OFFSET = ROBOT_IMAGE_SCALE * ROBOT_WIDTH_PIXEL / 2;

	private final Pane pane = new Pane();
	private final Polyline path = new Polyline();
	private final Polygon robot = new Polygon();
	private final Polyline simRobot = new Polyline(0,0,20,0,14,6,14,-6,20,0);
	private final Line lookAheadLine = new Line();
	private final Arc lookAheadArc = new Arc();
	private final Circle lookAheadPoint = new Circle();

	private final ImageView robotImage = new ImageView("RobotTop.png");

	private PathData pathsData;

	private double width, height;

	private double[] robotPose = new double[] {0,0,0};
	private double[] simRobotPose = new double[] {0,0,0};

	private double[] lookAhead = new double [] {0,0,0,0,0,0};

	public FieldView() {

		final NetworkTable table = NetworkTableInstance.getDefault().getTable("SmartDashboard");

		final NetworkTableEntry robotPoseEntry = table.getEntry("RobotPose");
		final NetworkTableEntry simRobotPoseEntry = table.getEntry("SimRobotPose");
		final NetworkTableEntry paths = table.getEntry("Paths");
		final NetworkTableEntry lookAheadEntry = table.getEntry("LookAhead");

		// Draw the field
		ImageView fieldImage = new ImageView("Field-2019.png");
		double w = fieldImage.getImage().getWidth();
		double h = fieldImage.getImage().getHeight();

		double l = 0.007;
		double r = 0.006;
		double t = 0.012;
		double b = 0.01;

		Rectangle2D rect = new Rectangle2D(l * w, t * h, (1.0 - l - r) * w, (1.0 - t - b) * h);
		fieldImage.setViewport(rect);
		fieldImage.setSmooth(true);
		fieldImage.setCache(true);
		fieldImage.setPreserveRatio(false);
		fieldImage.fitWidthProperty().bind(pane.widthProperty());
		fieldImage.fitHeightProperty().bind(heightProperty());
		pane.getChildren().add(fieldImage);

		// Draw a path
		path.setStroke(Color.RED);
		path.setStrokeType(StrokeType.CENTERED);
		pane.getChildren().add(path);
		path.toFront();

		// Draw the robot
		pane.getChildren().add(robotImage);
		getChildren().addAll(pane);
		drawRobot(robotPose[0], robotPose[1], robotPose[2]);

		// Draw the sim robot
		simRobot.setStroke(Color.RED);
		simRobot.setStrokeType(StrokeType.CENTERED);
		simRobot.setStrokeWidth(3.0);





		if(Boolean.getBoolean("simulated")) {
			pane.getChildren().add(simRobot);
			simRobot.toFront();
		}


		// Draw LookAhead curve
		lookAheadLine.setStroke(Color.PURPLE);
		lookAheadLine.setStrokeType(StrokeType.CENTERED);
		lookAheadLine.setFill(null);
		lookAheadLine.toFront();
		pane.getChildren().add(lookAheadLine);

		lookAheadArc.setType(ArcType.CHORD);
		lookAheadArc.setStroke(Color.PURPLE);
		lookAheadArc.setStrokeType(StrokeType.CENTERED);
		lookAheadArc.setFill(null);
		lookAheadArc.toFront();
		pane.getChildren().add(lookAheadArc);

		lookAheadPoint.setStroke(Color.PURPLE);
		lookAheadPoint.setFill(Color.PURPLE);
		lookAheadPoint.toFront();
		pane.getChildren().add(lookAheadPoint);


		robotPoseEntry.addListener(new Consumer<EntryNotification>() {

			@Override
			public void accept(EntryNotification t) {
				robotPose = t.getEntry().getDoubleArray((double[]) null);
				drawRobot(robotPose[0], robotPose[1], robotPose[2]);
			}

		}, EntryListenerFlags.kNew | EntryListenerFlags.kUpdate);

		simRobotPoseEntry.addListener(new Consumer<EntryNotification>() {

			@Override
			public void accept(EntryNotification t) {
				simRobotPose = t.getEntry().getDoubleArray((double[]) null);
				drawSimRobot(simRobotPose[0] / 0.0254, simRobotPose[1] / 0.0254, Math.toDegrees(simRobotPose[2]));
			}

		}, EntryListenerFlags.kNew | EntryListenerFlags.kUpdate);

		paths.addListener(new Consumer<EntryNotification>() {
			@Override
			public void accept(EntryNotification t) {
				byte[] rawData = t.getEntry().getRaw(null);
				pathsData = PathData.deserialize(rawData);
				drawPaths(pathsData);
			}
		}, EntryListenerFlags.kNew | EntryListenerFlags.kUpdate);

		lookAheadEntry.addListener(new Consumer<EntryNotification>() {

			@Override
			public void accept(EntryNotification t) {
				lookAhead = t.getEntry().getDoubleArray((double[]) null);
				drawLookaheadPath(lookAhead[0], lookAhead[1], lookAhead[2], lookAhead[3], lookAhead[4], lookAhead[5]);
			}

		}, EntryListenerFlags.kNew | EntryListenerFlags.kUpdate);

		width = pane.getWidth();
		height = pane.getHeight();
	}

	private void drawPaths(PathData pathsData) {
		if (pathsData == null) {
			return;
		}

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Translate translate = new Translate(0, height);
				Scale scale = new Scale(width / FIELD_LENGHT, -height / FIELD_WIDTH);

				path.getTransforms().clear();
				path.getTransforms().addAll(translate, scale);
				path.setStrokeWidth(1.0);

				path.getPoints().clear();
				for (double[] pathData : pathsData.getPaths()) {
					for (int i = 0; i < pathData.length;) {
						path.getPoints().add(pathData[i++]);
						path.getPoints().add(pathData[i++]);
					}
				}
			}
		});
	}


	private void drawRobot(final double x, final double y, final double hdg) {

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Scale scale = new Scale(width / FIELD_LENGHT, -height / FIELD_WIDTH);
				Translate translate = new Translate(0 + x, -FIELD_WIDTH + y);
				Rotate rotate = new Rotate(hdg);

				robot.getTransforms().clear();
				robot.getTransforms().addAll(scale, translate, rotate);

				Scale imageScale = new Scale(ROBOT_IMAGE_SCALE, ROBOT_IMAGE_SCALE);
				Translate imageTranslate = new Translate(-ROBOT_X_OFFSET, -ROBOT_Y_OFFSET);
				robotImage.getTransforms().clear();
				robotImage.getTransforms().addAll(scale, translate, rotate, imageTranslate, imageScale);
			}
		});
	}

	private void drawSimRobot(final double x, final double y, final double hdg) {

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Scale scale = new Scale(width / FIELD_LENGHT, -height / FIELD_WIDTH);
				Translate translate = new Translate(0 + x, -FIELD_WIDTH + y);
				Rotate rotate = new Rotate(hdg);

				simRobot.getTransforms().clear();
				simRobot.getTransforms().addAll(scale, translate, rotate);
			}
		});
	}

	public void drawLookaheadPath(double x, double y, double hdg, double curvature, double lookaheadX, double lookaheadY) {

		Platform.runLater(new Runnable() {

			@Override
			public void run() {

				boolean isStraight = true;//curvature == 0.0;
				lookAheadLine.setVisible(isStraight);
				lookAheadArc.setVisible(!isStraight);

				if (isStraight) {
					drawLine(x, y, lookaheadX, lookaheadY);
				} else {
					//drawLine(x, y, lookaheadX, lookaheadY);
					double direction =  Math.signum(curvature);
					double theta = hdg + direction * Math.PI / 2.0;
					double radius = 1.0 / Math.abs(curvature);
					double xCenter = Math.cos(theta)*radius + x;
					double yCenter = Math.sin(theta)*radius + y;
					double phi = Math.atan2(lookaheadY - yCenter, lookaheadX - xCenter);
					double startAngle = Math.toDegrees(theta - Math.PI);
					double arcAngle = Math.toDegrees(phi) - startAngle;
					//System.out.format("%6.3f %6.3f %6.3f %6.3f %6.3f %6.3f\n", Math.toDegrees(theta), radius, xCenter, yCenter, startAngle, arcAngle);

					lookAheadPoint.setCenterX(lookaheadX);
					lookAheadPoint.setCenterY(lookaheadY);


					drawArc(xCenter, yCenter, radius, startAngle, arcAngle);
				}
			}
		});
	}

	private void drawArc(double x, double y, double r, double startAngle, double arcAngle) {
		Scale scale = new Scale(width / FIELD_LENGHT, -height / FIELD_WIDTH);
		Translate translate = new Translate(0, -FIELD_WIDTH);

		lookAheadArc.setCenterX(x);
		lookAheadArc.setCenterY(y);
		lookAheadArc.setRadiusX(r);
		lookAheadArc.setRadiusY(r);
		lookAheadArc.setStartAngle(-startAngle);
		lookAheadArc.setLength(-arcAngle);
		lookAheadArc.setStrokeWidth(1.0);
		lookAheadArc.setType(ArcType.OPEN);

		lookAheadArc.getTransforms().clear();
		lookAheadArc.getTransforms().addAll(scale, translate);

		lookAheadPoint.setRadius(5.0);
		lookAheadPoint.getTransforms().clear();
		lookAheadPoint.getTransforms().addAll(scale, translate);
	}

	private void drawLine(double x, double y, double lookaheadX, double lookaheadY) {
		Scale scale = new Scale(width / FIELD_LENGHT, -height / FIELD_WIDTH);
		Translate translate = new Translate(0, -FIELD_WIDTH);

		lookAheadLine.setStartX(x);
		lookAheadLine.setStartY(y);
		lookAheadLine.setEndX(lookaheadX);
		lookAheadLine.setEndY(lookaheadY);
		lookAheadLine.setStrokeWidth(1.0);

		lookAheadLine.getTransforms().clear();
		lookAheadLine.getTransforms().addAll(scale, translate);
	}

	@Override
	public Node getView() {
		return this;
	}

	@Override
	public void updateData(long currentNanoTime) {
		double w = pane.getWidth();
		double h = pane.getHeight();
		if (w != width || h != height) {
			width = w;
			height = h;

			drawPaths(pathsData);
			drawRobot(robotPose[0], robotPose[1], robotPose[2]);
			drawSimRobot(simRobotPose[0], simRobotPose[1], simRobotPose[2]);
			drawLookaheadPath(lookAhead[0], lookAhead[1], lookAhead[2], lookAhead[3], lookAhead[4], lookAhead[5]);
		}
	}
}
